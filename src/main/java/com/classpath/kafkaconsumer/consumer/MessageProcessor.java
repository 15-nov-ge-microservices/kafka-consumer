package com.classpath.kafkaconsumer.consumer;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MessageProcessor {

    @KafkaListener(topics = "norris-message",groupId = "message-consumer")
    public void processMessage(String message){
        log.info("Processing the message from the topic :: {} ", message);
    }

}